"use strict";
import "./main.scss"
import $ from "jquery"
import page from "page"
import home from "./pages/home"
import region from "./pages/region"
import directions from "./pages/directions"
import notFound from "./pages/notfound"
import ScrollMagic from "scrollmagic"
import "ScrollToPlugin"


page('/', home, initScrollNav);
page('/region_contest', region, initScrollNav);
page('/contest_directions', directions, initScrollNav);
page('*', notFound);

page.start();


/**
 *
 */
function initScrollNav() {
	let controller = new ScrollMagic.Controller({
		globalSceneOptions: {
			// duration: $('section').height(),
			reverse: true
		}
	});

	controller.scrollTo(function (target) {

		TweenMax.to(window, 0.5, {
			scrollTo: {
				y: target,
				autoKill: true // Allow scroll position to change outside itself
			},
			ease: Cubic.easeInOut
		});

	});

	let anchors = [];
	$('[href^="#"]').each(function () {
		let anchor = $(this).attr('href');

		if (!anchors.find(a => a === anchor)) {
			anchors.push(anchor);
			new ScrollMagic
				.Scene({
				duration: $(anchor).height(),
				triggerHook: 0.25,
				triggerElement: anchor,
			})
				.setClassToggle(`[href="${anchor}"]`, 'active')
				.addTo(controller);
		}
	});

	let last_id = $("main section:last-of-type")[0].id;
	console.log(last_id);

	new ScrollMagic
		.Scene({
		triggerElement: '#footer',
		triggerHook: "onEnter",
		duration: $('#footer').height() + 1,
	})
		.setClassToggle(`.SideNav`, 'scrolledToBottom')
		.addTo(controller);

	$(document).on("click", "a[href*='#']", function (e) {
		let href = $(this).attr("href"),
			id = '#' + href.split('#')[1];

		if ($(id).length > 0) {
			e.preventDefault();

			// trigger scroll
			controller.scrollTo(id);

			// If supported by the browser we can also update the URL
			if (window.history && window.history.pushState) {
				history.pushState("", document.title, id);
			}
		}

	});
}