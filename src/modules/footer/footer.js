import $ from "jquery"

import template from "./footer.html"
import "./footer.scss"

export default class Footer {

	static init() {
		$('main').append(template);

		let input = $('footer form input');

		input.focus(() => {
			input.parent().addClass('active')
		});

		input.blur(() => {
			input.parent().removeClass('active')
		})
	}
}
