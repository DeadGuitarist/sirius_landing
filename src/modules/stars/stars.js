import $ from "jquery"
import * as THREE from "three"
import TweenMax from 'gsap'
import "./stars.scss"
import template from "./stars.html"

let container;
let camera;

let scene;

let textureLoader = new THREE.TextureLoader();
let renderer = new THREE.WebGLRenderer();
let geometry = new THREE.Geometry();

let materials = [];

let mouseX = 0;
let mouseY = 0;

let windowHalfX = window.innerWidth / 2;
let windowHalfY = window.innerHeight / 2;

let particlesNumber = 150;

let exploding = false;


export default class Stars {

	static init() {
		$('main').append(template);
		container = $('.Stars-container');

		camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1200);
		camera.position.z = 1000;

		scene = new THREE.Scene();
		scene.fog = new THREE.FogExp2(0x000000, 0.0008);


		setTimeout(() => {
			Stars.start();
			Stars.animate()
		}, 800)
	}

	/**
	 *
	 */
	static start() {
		let sprite = require('./sprite.png');
		let sprite1 = textureLoader.load(sprite),
			sprite2 = textureLoader.load(sprite),
			sprite3 = textureLoader.load(sprite),
			sprite4 = textureLoader.load(sprite),
			sprite5 = textureLoader.load(sprite);

		for (let i = 0; i < particlesNumber; i++) {
			var vertex = new THREE.Vector3();

			vertex.x = 0;
			vertex.y = 0;
			vertex.z = 0;

			geometry.vertices.push(vertex);
		}


		let parameters = [
			[[.8, 0.79, 1], sprite2, 10],
			[[.8, 0.79, 1], sprite3, 10],
			[[.8, 0.79, 1], sprite1, 10],
			[[.8, 0.79, 1], sprite5, 10],
			[[.8, 0.79, 1], sprite4, 10]
		];
		for (let i = 0; i < parameters.length; i++) {

			let color = parameters[i][0];
			let sprite = parameters[i][1];
			let size = parameters[i][2];

			materials[i] = new THREE.PointsMaterial({
				size: size,
				map: sprite,
				depthTest: false,
				transparent: true
			});

			materials[i].color.setHSL(color[0], color[1], color[2]);

			let particles = new THREE.Points(geometry, materials[i]);
			particles.rotation.x = Math.random() * 0.01;
			particles.rotation.y = Math.random() * 0.01;
			particles.rotation.z = Math.random() * 0.01;

			scene.add(particles);

		}

		for (let i = 0; i < particlesNumber; i++) {

			TweenMax.fromTo(geometry.vertices[i], 2,
				{
					x: 0,
					y: 0,
					z: 0
				}, {
					y: Math.random() * 2000 - 1000,
					x: Math.random() * 2000 - 1000,
					z: Math.random() * 2000 - 1000,
					ease: Power4.easeOut
				});

		}

		Stars.animateExplosion();

		renderer.setPixelRatio(window.devicePixelRatio);
		renderer.setSize(window.innerWidth, window.innerHeight);
		renderer.setClearColor( 0x48255b, 1);
		container.append(renderer.domElement);

		$(document).mousemove(Stars.onDocumentMouseMove);
		$(window).resize(Stars.onWindowResize);
	}


	/**
	 *
	 */
	static onWindowResize() {
		windowHalfX = window.innerWidth / 2;
		windowHalfY = window.innerHeight / 2;
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize(window.innerWidth, window.innerHeight);
	}


	/**
	 *
	 * @param event
	 */
	static onDocumentMouseMove(event) {
		mouseX = event.clientX - windowHalfX;
		mouseY = event.clientY - windowHalfY;
	}

	/**
	 *
	 */
	static animate() {
		requestAnimationFrame(Stars.animate);
		Stars.render();
	}


	/**
	 *
	 */
	static animateExplosion() {
		geometry.verticesNeedUpdate = true;
		requestAnimationFrame(Stars.animateExplosion);
	}


	/**
	 *
	 */
	static render() {

		let time = Date.now() * 0.00005;

		// camera.position.x += ( mouseX - camera.position.x ) * 0.005;
		camera.position.y += ( -mouseY - camera.position.y ) * 0.005;

		camera.lookAt(scene.position);

		for (let i = 0; i < scene.children.length; i++) {
			var object = scene.children[i];

			if (object instanceof THREE.Points) {
				object.rotation.y = time * ( i < 4 ? i + 1 : -( i + 1 ) ) * 0.2;
			}
		}

		renderer.render(scene, camera);

	}
}

