export const MENUS = {
	"/": [
		{name: "Конкурс", href: "#main"},
		{name: "Направления", href: "/contest_directions#main"},
		{name: "Новости", href: "#news"},
		{name: "Условия", href: "#conditions"},
		{name: "Конкурсы регионов", href: "#participants"},
		{name: "Партнеры", href: "#partners"}
	],
	"/region_contest": [
		{name: "Конкурс", href: "#main"},
		{name: "Направления", href: "#directions"},
		{name: "Новости", href: "#news"},
		{name: "Этапы", href: "#stages"},
		{name: "Организация конкурса", href: "#organization"}
	],
	"/contest_directions": [
		{name: "Конкурс", href: "#main"},
		{name: "Беспилотники", href: "#drones"},
		{name: "Космос", href: "#space"},
		{name: "Транспорт", href: "#auto_cars"},
		{name: "Энергетика", href: "#energy"},
		{name: "Безопасность", href: "#security"},
		{name: "Технологии", href: "#tech"},
		{name: "Машинное обучение", href: "#machine_learning"},
		{name: "Медицина", href: "#medicine"}
	]
};


