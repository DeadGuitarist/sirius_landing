import $ from "jquery"
import * as Cookie from "js-cookie"
import template from "./header.html"
import "./header.scss"
import {REGIONS, CONTEST_REGIONS} from "modules/region_changer/regions"
import {MENUS} from "./menus"
import * as page from "page"

export default class Header {

	/**
	 *
	 * @param path
	 */
	static init(path) {
		$('main').append(template);
		let region = Cookie.getJSON('region');

		MENUS[path].forEach(url => {
			$('.Header .top-menu').append(`
				<li><a href="${url.href}">${url.name}</a></li>		  
			`);
		});

		$('.Header .region-container a.contest_page_link').click((e) => {
			e.preventDefault();
			Cookie.remove('tmp_region');
			page.redirect('/region_contest#main')
		});


		// todo вынести получение региона в RegionChanger
		if (region) {
			if (path !== '/') {
				$('.Header .region-container').hide();
				$('.Header .back-button').show();
			}
			Header.changeRegion(region);
		} else {
			Header.fetchRegion()
				.then(data => {
					let region = REGIONS.find(region => `${data.countryCode}-${data.region}` === region.code);
					if (!region)
						region = REGIONS.find(region => `RU-MOW` === region.code);

					Header.changeRegion(region);
					Header.openRegionSuggest(region);

					if (path !== '/') {
						$('.Header .region-container').hide();
						$('.Header .back-button').show();
					}
				});
		}
	}


	/**
	 *
	 * @param region
	 */
	static openRegionSuggest(region) {
		let popup = require('./suggest_region.html');
		$('.Header .region-container').append(popup);

		popup = $('.suggest-popup');
		$('.suggest-popup .suggest-region-name').text(region.name);
		$('.suggest-popup .btns .ok').click(() => {
			popup.remove()
		});

		$('.suggest-popup .btns .select-region').click(() => {
			popup.remove();
			$('.RegionChanger').css('top', '0');
			$('body').css('overflow', 'hidden')
		})
	}


	/**
	 *
	 */
	static fetchRegion() {
		return $.getJSON("http://ip-api.com/json?lang=ru&fields=region,countryCode");
	}

	/**
	 *
	 * @param region
	 */
	static changeRegion(region = Cookie.getJSON('region')) {
		let region_flag = require(`flags/${region.code}`);
		$('.region-flag').show().attr({
			src: region_flag,
			alt: region.code
		});

		$('.region-selector').text(region.name).click(event => {
			event.preventDefault();
			$('.RegionChanger').css('top', '0');
			$('body').css('overflow', 'hidden')
		});

		if (CONTEST_REGIONS.find(c_region => c_region.code === region.code)) {
			$('.region-contest').css('display', 'flex')
		} else {
			$('.region-contest').css('display', 'none')
		}

		// Cookie.remove('tmp_region');

		Cookie.set('region', {
			name: region.name,
			code: region.code,
			flag_src: region_flag
		});

	}
}
