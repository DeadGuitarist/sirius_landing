import $ from "jquery"
import Cookie from "js-cookie"
import template from "./region_changer.html"
import "./region_changer.scss"
import {REGIONS} from "./regions"
import Header from "../header/header"

export default class RegionChanger {

	static init() {
		let interval = setInterval(() => {
			let region = Cookie.getJSON('region');
			if(region) {
				$('main').append(template);
				$('.RegionChanger .current-region a').text(region.name);
				RegionChanger.setGridData();
				RegionChanger.setLinksEvents();
				RegionChanger.setInputEvent();
				RegionChanger.setCloseEvent();
				clearInterval(interval);
			}
		}, 200);

	}

	static setCloseEvent() {
		$('.RegionChanger .close-button').click(() => {
			$('.RegionChanger input').val("");
			RegionChanger.setGridData();
			RegionChanger.setLinksEvents();

			$('.RegionChanger').css('top', '100vh');
			$('body').css('overflow', 'auto')
		})
	}

	/**
	 *
	 */
	static setInputEvent() {
		$('.RegionChanger input').keyup(function (event) {
			let search_text = $(this).val().toLowerCase(),
				filtered = REGIONS.filter(region => region.name.toLowerCase().search(search_text) !== -1);
			RegionChanger.setGridData(filtered);
			RegionChanger.setLinksEvents();
		})
	}


	/**
	 *
	 */
	static setGridData(regions = REGIONS) {
		let grid = $('.RegionChanger .inner .grid');
		let grid_data = {};

		grid.empty();

		regions.sort((a, b) => {
			if (a.name > b.name) {
				return 1;
			}
			if (a.name < b.name) {
				return -1;
			}
			// a должно быть равным b
			return 0;
		});

		regions.forEach(region => {
			if (!grid_data[region.name[0].toLowerCase()]) {
				grid_data[region.name[0].toLowerCase()] = [...regions.filter(r => r.name[0].toLowerCase() === region.name[0].toLowerCase())]
			}
		});

		for (let key in grid_data) {
			grid.append(`
				<div data-letter="${key}" class="regions-list">
					<span class="main_liter">${key}</span>
					<div class="sub-grid"></div>
				</div>
			`);
			let sub_grid = grid.find(`[data-letter="${key}"] .sub-grid`);

			grid_data[key].forEach(item => {
				sub_grid.append(`
					<a href="" data-region-code="${item.code}">${item.name}</a>
				`)
			});

		}
	}


	/**
	 *
	 */
	static setLinksEvents() {
		let links = $('.RegionChanger .grid a');

		links.click(function (event) {
			event.preventDefault();

			$('.RegionChanger input').val("");
			RegionChanger.setGridData();
			RegionChanger.setLinksEvents();

			let code = $(this).data('regionCode'),
				region = REGIONS.find(region => region.code === code);
			Cookie.set('region', region);
			Header.changeRegion(region);

			$('.RegionChanger').css('top', '100vh');
			$('body').css('overflow', 'auto')
		});
	}

	/**
	 *
	 * @returns {*}
	 */
	static getCurrentRegion() {
		let region = Cookie.getJSON('region'),
			tmp_region = Cookie.getJSON('tmp_region');

		return tmp_region || region
	}
}
