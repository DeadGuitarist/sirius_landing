import $ from "jquery"
import template from "./contest_winners.html"
import "./contest_winners.scss"


export default class ContestWinners {

	static init() {
		$('main').append(template);

		$(`.ContestWinners button`).click(() => {
			window.open(`/files/conditions.pdf`)
		});
	}
}

