import $ from "jquery"

import template from "./contest_main.html"
import "./contest_main.scss"

export default class ContestMain {

	static init() {
		$('main').append(template);
	}
}

