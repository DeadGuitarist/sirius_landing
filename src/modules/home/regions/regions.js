import $ from "jquery"
import Cookie from "js-cookie"
import page from "page"
import template from "./regions.html"
import "./regions.scss"
import {REGIONS} from "../../region_changer/regions"

export default class Regions {

	static init() {
		$('main').append(template);
		Regions.initLinks()
	}

	static initLinks() {
		$('.Regions .item a').click(function(e) {
			e.preventDefault();
			let region = REGIONS.find(r => r.code === $(this).data('region'));
			Cookie.set('tmp_region', region);
			page.redirect('/region_contest#main');
			window.scroll(0, 0)
		})
	}
}

