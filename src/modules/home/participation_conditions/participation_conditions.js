import $ from "jquery"
import template from "./participation_conditions.html"
import "./participation_conditions.scss"

const GRID = [
	{img: require('./images/pic-01-header.svg'), text: "Беспилотные<br>летательные аппараты", href: '/contest_directions#drones'},
	{img: require('./images/pic-02-header.svg'), text: "Космические<br>технологии", href: "contest_directions#space"},
	{img: require('./images/pic-03-header.svg'), text: "Энергетические<br>системы", href: "contest_directions#energy"},
	{img: require('./images/pic-04-header.svg'), text: "Автономный<br>транспорт", href: "contest_directions#auto_cars"},

	{img: require('./images/pic-06-header.svg'), text: "Безопасность<br>человека", href: "contest_directions#security"},
	{img: require('./images/pic-05-header.svg'), text: "Большие данные<br>и машинное обучение", href: "contest_directions#machine_learning"},
	{img: require('./images/pic-08-header.svg'), text: "Современные технологии<br>в сельском хозяйстве", href: "contest_directions#tech"},
	{img: require('./images/pic-07-header.svg'), text: "Персональная<br>медицина", href: "contest_directions#medicine"}
];

export default class ParticipationConditions {

	static init() {
		$('main').append(template);

		let grid = $('.ParticipationConditions .grid');
		GRID.forEach(item => {
			grid.append(`
				<div class="item">
					<a href="${item.href}">
						<div class="img-container"><img src="${item.img}" /></div>
						<h5>${item.text}</h5>
					</a>
				</div>
			`);
		});

		$(`.ParticipationConditions button`).click(() => {
			window.open(`/files/structure.pdf`)
		});
	}
}

