import $ from "jquery"
import template from "./directions.html"
import "./directions.scss"

export default class ContestDirections {

	static init() {
		$('main').append(template);
	}
}

