import $ from "jquery"
import template from "./main.html"
import "./main.scss"

export default class ContestDirectionsMain {

	static init() {
		$('main').append(template);
	}
}

