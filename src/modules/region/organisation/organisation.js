import $ from "jquery"
import template from "./organisation.html"
import "./organisation.scss"
import RegionChanger from "modules/region_changer/region_changer"
import {FILES} from "./files"

export default class Organisation {


	/**-.
	 *
	 */
	static init() {
		$('main').append(template);
		Organisation.createGrid();
	}


	static createGrid() {
		let region = RegionChanger.getCurrentRegion();
		let files = FILES[region.code];

		let grid = $('.Organisation .grid');

		if (files)
			files.forEach(file => {
				grid.append(`
					<a href="/${file.url}" target="_blank">
						
						<div>
							<span>${file.name}</span>
							<span class="small-text">${file.file_name}</span>					
						</div>
					</a>
				`)

			})
	}
}

