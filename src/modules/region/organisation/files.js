export const FILES = {
	"RU-LIP": [
		{name: 'Оргкомитет', file_name: 'Оргкомитет (Липецк).pdf', url: 'files/RU-LIP/1.pdf'},
		{name: 'Экспертный совет', file_name: 'Экспертный совет (Липецк).pdf', url: 'files/RU-LIP/2.pdf'},
		{name: 'Перечень направлений конкурса', file_name: 'Перечень направлений конкурса (Липецк).pdf', url: 'files/RU-LIP/8.pdf'},
		{name: 'Положение Конкурса - проект', file_name: 'Положение Конкурса - проект (Липецк).pdf', url: 'files/RU-LIP/3.pdf'},
		{name: 'Предполагаемые партнеры конкурса', file_name: '9. Предполагаемые партнеры конкурса (Липецк).pdf', url: 'files/RU-LIP/9.pdf'},
		{name: 'Перечень площадок', file_name: 'Перечень площадок (Липецк).pdf', url: 'files/RU-LIP/5.pdf'}
	],
	"RU-KHM": [
		{name: 'Перечень направлений', file_name: 'Перечень направлений (ХМАО).pdf', url: 'files/RU-KHM/1.pdf'},
		{name: 'Перечень площадок', file_name: 'Перечень площадок (ХМАО).pdf', url: 'files/RU-KHM/2.pdf'},
		{name: 'Положение о проведении конкурса', file_name: 'Положение о проведении конкурса (ХМАО).pdf', url: 'files/RU-KHM/3.pdf'},
	],
	"RU-TYU": [
		{name: 'Оргкомитет', file_name: 'Оргкомитет Тюменская область.pdf', url: 'files/RU-TYU/1.pdf'},
		{name: 'Партнеры конкурса', file_name: 'Партнеры конкурса.pdf', url: 'files/RU-TYU/2.pdf'},
		{name: 'Экспертный совет', file_name: 'Экспертный совет Тюменская область.pdf', url: 'files/RU-TYU/3.pdf'},
		{name: 'Перечень направлений конкурса', file_name: 'Перечень направлений конкурса.pdf', url: 'files/RU-TYU/4.pdf'},
		{name: 'Положение ВКПР', file_name: 'Положение_ВКПР Тюменская область.pdf', url: 'files/RU-TYU/5.pdf'},
		{name: 'Перечень площадок', file_name: 'Перечень площадок проведения конкурса Тюменская область.pdf', url: 'files/RU-TYU/6.pdf'}
	],
	"RU-TUL": [
		{name: 'Положение о проведении', file_name: 'Положение Тульская область.pdf', url: 'files/RU-TUL/1.pdf'},
		{name: 'Экспертный совет', file_name: 'Экспертный совет Тульская область.pdf', url: 'files/RU-TUL/2.pdf'},
	],
	"RU-KLU": [
		{name: 'Перечень направлений конкурса', file_name: 'Направления конкурса.pdf', url: 'files/RU-KLU/1.pdf'},
		{name: 'Оргкомитет', file_name: 'Оргкомитет (Калужская область).pdf', url: 'files/RU-KLU/2.pdf'},
		{name: 'Перечень площадок', file_name: 'Перечень региональных опорных площадок (Калужская область).pdf', url: 'files/RU-KLU/3.pdf'},
		{name: 'Положение о проведении', file_name: 'Положение (Калужская область).pdf', url: 'files/RU-KLU/4.pdf'}
	],
	"RU-BEL": [
		{name: 'Перечень направлений конкурса', file_name: 'Направления (Белогородская область).pdf', url: 'files/RU-BEL/1.pdf'},
		{name: 'Оргкомитет', file_name: 'Оргкомитет (Белгородская область).pdf', url: 'files/RU-BEL/2.pdf'},
		{name: 'Партнеры конкурса', file_name: 'Партнеры (Белгородская область).pdf', url: 'files/RU-BEL/3.pdf'},
		{name: 'Положение о проведении', file_name: 'Положение (Белгородская область).pdf', url: 'files/RU-BEL/4.pdf'},
		{name: 'Экспертный совет', file_name: 'Экспертный совет (Белгородская область).pdf', url: 'files/RU-BEL/5.pdf'}
	],
	"RU-VLA": [
		{name: 'Перечень направлений конкурса', file_name: 'Направления конкурса (Владмирская область).pdf', url: 'files/RU-VLA/1.pdf'},
		{name: 'Партнеры конкурса', file_name: 'Партнеры (Владимирская область).pdf', url: 'files/RU-VLA/2.pdf'},
		{name: 'Перечень площадок', file_name: 'Перечень площадок (Владимирская область).pdf', url: 'files/RU-VLA/3.pdf'},
		{name: 'Положение о проведении', file_name: 'Положение (Владимирская область).pdf', url: 'files/RU-VLA/4.pdf'}
	]
};
