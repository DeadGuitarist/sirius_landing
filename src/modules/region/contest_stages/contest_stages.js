import $ from "jquery"
import RegionChanger from "../../region_changer/region_changer"
import template from "./contest_stages.html"
import "./contest_stages.scss"
import {STAGES} from "./stages"


export default class ContestStages {

	static init() {
		$('main').append(template);

		let region = RegionChanger.getCurrentRegion();

		STAGES[region.code].forEach((stage, index) => {
			$(".ContestStages .stages-cards").append(`
				<div class="card" data-card-number="${index + 1}">
					<h4>${stage.name} ${stage.date ? `(${stage.date}).` : ""}</h4>
					${stage.text}
					<div class="btns">
						<button class="transparent tasks">СКАЧАТЬ ЗАДАНИЯ ЭТАПА</button>
						<button class="transparent results">СКАЧАТЬ РЕЗУЛЬТАТЫ</button>
					</div>
				</div>`);

			$(`.ContestStages .stages-cards [data-card-number=${index + 1}] button.tasks`).click(() => {
				window.open(`/files/${region.code}/tasks.pdf`)
			});

			$(".ContestStages .stages-cards-controls").append(`
				<span data-card-number="${index + 1}">${stage.name} <br> ${stage.date || ''}</span>			
			`)
		});

		ContestStages.initCards();
	}


	/**
	 *
	 */
	static initCards() {
		$('.ContestStages .stages-cards .card[data-card-number="1"]').show().addClass('active');
		$('.ContestStages .stages-cards-controls span[data-card-number="1"]').addClass('active');

		$('.ContestStages .stages-cards-controls span').each(function () {
			let self = $(this);

			self.click(() => {
				$(`.ContestStages .stages-cards .card.active`).removeClass('active').fadeOut(275, () => {
					$(`.ContestStages .stages-cards .card[data-card-number="${self.data('cardNumber')}"]`).fadeIn(275).addClass('active');
				});


				$(`.ContestStages .stages-cards-controls span`).removeClass('active');
				self.addClass('active')
			})
		})
	}
}

