import $ from "jquery"
import RegionChanger from "modules/region_changer/region_changer"
import template from "./main.html"
import "./main.scss"

export default class RegionMain {

	static init() {
		$('main').append(template);

		let region = RegionChanger.getCurrentRegion();

		let region_coat = require(`coats/${region.code}`);

		let coat_element = $('.RegionMain .current-region-coat');
		coat_element.attr('src', region_coat);
		$('.RegionMain .current-region-name').text(region.name);

		if (region.code === "RU-KHM") {
			coat_element.css('margin-top', '90px')
		}
	}
}

