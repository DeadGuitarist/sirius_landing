import $ from "jquery"

import template from "./news.html"
import "./news.scss"
import {NEWS} from "./news-mock"


export default class News {

	static init() {
		$('main').append(template);

		let grid = $('.News .grid');
		NEWS.forEach(item => {
			grid.append(`
				<div class="item">
					<img src="${item.image}" alt="">
					<div class="text">
						<span class="news-type">${item.type}</span>
						<h4>${item.title}</h4>
						<span class="small-text">${item.date}</span>
						<p>${item.text}</p>
					</div>
				</div>			
			`)
		})
	}
}

