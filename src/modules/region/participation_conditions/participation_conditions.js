import $ from "jquery"
import RegionChanger from "../../region_changer/region_changer"
import template from "./participation_conditions.html"
import "./participation_conditions.scss"
import {GRID} from "./conditions"

export default class ParticipationConditions {

	static init() {
		$('main').append(template);

		let grid = $('.ParticipationConditionsRegion .grid');
		let line_items_count;

		let region = RegionChanger.getCurrentRegion()

		switch (GRID[region.code].length) {
			case 6:
			case 5:
			case 3:
				line_items_count = 3;
				break;
			case 8:
			case 4:
				line_items_count = 4;
				break;
			case 2:
				line_items_count = 2;
				break;
			case 1:
				line_items_count = 1
		}

		GRID[region.code].forEach((item, index) => {
			grid.append(`
				<div class="item">
					<div class="img-container"><img src="${item.img}" /></div>
					<h5>${item.title}</h5>
					${item.text ? `<p>${item.text}</p>` : ""}
				</div>
			`);
			if (((index + 1) % line_items_count) === 0) {
				grid.append(`<div class="line-break"></div>`);
			}
		});
	}
}

