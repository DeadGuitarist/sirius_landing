export const GRID = {
	"RU-LIP": [
		{
			img: require('./images/pic-06-header.svg'),
			title: "Безопасность<br>человека",
			text: 'Небольшое описание направления конкурса. Vivamus adipiscing fermentum quam volutpat aliquam. Integer et elit eget elit facilisis tristique. Nam vel iaculis mauris. Sed ullamcorper tellus erat, non ultrices sem tincidunt euismod. Fusce rhoncus porttitor velit, eu bibendum nibh aliquet vel.'
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Большие данные<br>и машинное обучение",
			text: 'Небольшое описание направления конкурса. Vivamus adipiscing fermentum quam volutpat aliquam. Integer et elit eget elit facilisis tristique. Nam vel iaculis mauris. Sed ullamcorper tellus erat, non ultrices sem tincidunt euismod. Fusce rhoncus porttitor velit, eu bibendum nibh aliquet vel.'
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Машиностроение",
			text: 'Небольшое описание направления конкурса. Vivamus adipiscing fermentum quam volutpat aliquam. Integer et elit eget elit facilisis tristique. Nam vel iaculis mauris. Sed ullamcorper tellus erat, non ultrices sem tincidunt euismod. Fusce rhoncus porttitor velit, eu bibendum nibh aliquet vel.'
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Современные технологии<br>в сельском хозяйстве",
			text: 'Небольшое описание направления конкурса. Vivamus adipiscing fermentum quam volutpat aliquam. Integer et elit eget elit facilisis tristique. Nam vel iaculis mauris. Sed ullamcorper tellus erat, non ultrices sem tincidunt euismod. Fusce rhoncus porttitor velit, eu bibendum nibh aliquet vel.'
		}
	],
	"RU-KHM": [
		{
			img: require('./images/pic-01-header.svg'), // todo поменять картинку
			title: "Экология жизни",
		},
		{
			img: require('./images/pic-01-header.svg'),
			title: "Беспилотные летательные аппараты, робототехника",
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Автономный транспорт",
		},
		{
			img: require('./images/pic-03-header.svg'),
			title: "Распределенные энергетические системы (умная энергетика)",
		},
		{
			img: require('./images/pic-06-header.svg'),
			title: "Персональные системы безопасности (умный дом, интеллектуальные системы)",
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Персональная медицина",
		}

	],
	"RU-TYU": [
		{
			img: require('./images/pic-04-header.svg'),
			title: "Автономный транспорт",
		},
		{
			img: require('./images/pic-03-header.svg'),
			title: "Энергетические системы",
		},
		{
			img: require('./images/pic-06-header.svg'),
			title: "Безопасность человека",
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Современные технологии в сельском хозяйстве",
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Большие данные и машинное обучение",
		},
		{
			img: require('./images/pic-01-header.svg'),
			title: "Беспилотные летательные аппараты",
		},
		{
			img: require('./images/pic-07-header.svg'),
			title: "Персональная медицина",
		},
		{
			img: require('./images/pic-08-header.svg'), // todo поменять картинку
			title: "Современные технологии в нефтедобывающей и нефтеперерабатывающей промышленности",
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Биоинформационные и нейротехнологии",
		}
	],
	"RU-TUL": [
		{
			img: require('./images/pic-01-header.svg'),
			title: "Беспилотные летательные аппараты",
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Автономный транспорт",
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Современные технологии в сельском хозяйстве",
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Большие данные и машинное обучение",
		},
		{
			img: require('./images/pic-07-header.svg'),
			title: "Персональная медицина",
		}
	],
	"RU-KLU": [
		{
			img: require('./images/pic-06-header.svg'),
			title: "Охрана окружающей среды и экологическая безопасность",
		},
		{
			img: require('./images/pic-02-header.svg'),
			title: "Развитие аэрокосмических технологий",
		},
		{
			img: require('./images/pic-07-header.svg'),
			title: "Биотехнологии и медицина",
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Туристско-рекреационный потенциал региона",
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Компьютерные технологии",
		}
	],
	"RU-BEL": [
		{
			img: require('./images/pic-01-header.svg'),
			title: "Беспилотные летательные аппараты",
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Автономный транспорт",
		},
		{
			img: require('./images/pic-03-header.svg'),
			title: "Распределенные энергетические системы",
		},
		{
			img: require('./images/pic-03-header.svg'),  // todo сменить картинку
			title: "Разработка и применение новых материалов, нанотехнологии",
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Большие данные и машинное обучение",
		},
	],
	"RU-VLA": [
		{
			img: require('./images/pic-01-header.svg'),
			title: "Беспилотные летательные аппараты",
		},
		{
			img: require('./images/pic-02-header.svg'),
			title: "Космические технологии",
		},
		{
			img: require('./images/pic-04-header.svg'),
			title: "Автономный транспорт",
		},
		{
			img: require('./images/pic-03-header.svg'),
			title: "Распределенные энергетические системы",
		},
		{
			img: require('./images/pic-06-header.svg'),
			title: "Безопасность человека",
		},
		{
			img: require('./images/pic-08-header.svg'),
			title: "Современные технологии в сельском хозяйстве",
		},
		{
			img: require('./images/pic-05-header.svg'),
			title: "Большие данные и машинное обучение",
		},
		{
			img: require('./images/pic-07-header.svg'),
			title: "Персональная медицина",
		},
	]
};
