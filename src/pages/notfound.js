import $ from "jquery"
import Footer from "../modules/footer/footer"

export default function Home() {
	$('main')
		.empty()
		.append(`
			<div>
	            <h2>404 Такой страницы нет :(</h2>			
			</div>
		`);

	Footer.init();
}