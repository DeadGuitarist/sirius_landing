import Stars from "../modules/stars/stars"
import Header from "../modules/header/header"
import Footer from "../modules/footer/footer"
import SideNav from "../modules/contest_directions/side_nav/side_nav"
import $ from "jquery"
import ContestDirectionsMain from "../modules/contest_directions/main/main"
import ContestDirections from "../modules/contest_directions/directions/directions"

export default function Directions(ctx, next) {
	$('main').empty();

	Stars.init();
	Header.init(ctx.path);
	SideNav.init();

	ContestDirectionsMain.init();
	ContestDirections.init();


	Footer.init();

	setTimeout(next, 0)

}



