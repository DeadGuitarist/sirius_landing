import Stars from "../modules/stars/stars"
import Header from "../modules/header/header"
import ContestMain from "../modules/home/contest_main/contest_main"
import ParticipationConditions from "../modules/home/participation_conditions/participation_conditions"
import News from "../modules/home/news/news"
import ContestWinners from "../modules/home/contest_winners/contest_winners"
import Regions from "../modules/home/regions/regions"
import Partners from "../modules/home/partners/partners"
import Footer from "../modules/footer/footer"
import SideNav from "../modules/home/side_nav/side_nav"
import $ from "jquery"
import RegionChanger from "../modules/region_changer/region_changer"

export default function Home(ctx, next) {
	$('main').empty();

	Stars.init();
	Header.init(ctx.path);
	SideNav.init();

	ContestMain.init();
	ParticipationConditions.init();
	News.init();
	ContestWinners.init();
	Regions.init();
	Partners.init();

	RegionChanger.init();

	Footer.init();

	next()
}

