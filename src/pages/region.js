import Stars from "../modules/stars/stars"
import Header from "../modules/header/header"
import Footer from "../modules/footer/footer"
import SideNav from "../modules/region/side_nav/side_nav"
import $ from "jquery"
import RegionMain from "../modules/region/main/main"
import ParticipationConditions from "../modules/region/participation_conditions/participation_conditions"
import News from "../modules/region/news/news"
import ContestStages from "../modules/region/contest_stages/contest_stages"
import Organisation from "../modules/region/organisation/organisation"

export default function Region(ctx, next) {
	$('main').empty();

	Stars.init();
	Header.init(ctx.path);
	SideNav.init();

	RegionMain.init();
	ParticipationConditions.init();
	News.init();
	ContestStages.init();
	Organisation.init();

	Footer.init();

	setTimeout(next, 0)

}



