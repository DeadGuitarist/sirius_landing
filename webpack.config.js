const
	webpack = require("webpack"),
	CopyWebpackPlugin = require('copy-webpack-plugin'),
	CleanWebpackPlugin = require('clean-webpack-plugin'),
	ExtractTextPlugin = require('extract-text-webpack-plugin'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	path = require('path'),
	autoprefixer = require('autoprefixer');


const gsapPath = "node_modules/gsap/src/uncompressed/";

let config = {
	entry: {
		main: path.resolve(__dirname, 'src/main.js'),
		polyfills: path.resolve(__dirname, 'src/polyfills.js')
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: "[name].[hash].js"
	},
	resolve: {
		root: path.resolve(__dirname, 'src'),
		alias: {
			flags: path.resolve(__dirname, 'src/images/flags'),
			coats: path.resolve(__dirname, 'src/images/coats'),
			partners: path.resolve(__dirname, 'src/images/partner-logos'),
			directions: path.resolve(__dirname, 'src/images/directions'),
			images: path.resolve(__dirname, 'src/images'),
			modules: path.resolve(__dirname, 'src/modules'),
			src: path.resolve(__dirname, 'src'),
			TweenLite: "gsap",
			ScrollToPlugin: path.resolve(__dirname, gsapPath + "plugins/ScrollToPlugin.js")
		},
		extensions: ['.js', '.scss', '.svg', '.png', '']
	},
	devtool: 'module-source-map',
	progress: true,
	colors: true,
	module: {
		loaders: [
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract(
					'style/url!file',
					'!css!sass?sourceMap!postcss'
				)
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel',
				compact: true,
				query: {
					presets: ['es2015']
				}
			},
			{
				test: /\.html$/,
				loader: 'html'
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file'
			},
			{
				test: /\.(eot|ttf|woff|woff2)$/,
				loader: 'file?limit=500000'
			}

		]
	},
	postcss: [autoprefixer({browsers: ['last 3 versions']})],
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html'
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common',
			filename: "common.[hash].js"
		}),
		new CopyWebpackPlugin([
			{from: path.resolve(__dirname, 'src/fonts')},
			{from: path.resolve(__dirname, 'src/files'), to: 'files'}
			// {from: './src/images', to: 'images'}
		]),
		new CleanWebpackPlugin(['build']),
		new ExtractTextPlugin('[name].[hash].css', {allChunks: true}),
		new webpack.ProvidePlugin({
			TweenMax: "gsap"
		})
	],
	devServer: {
		contentBase: path.resolve(__dirname, "build"),
		historyApiFallback: {
			index: '/',
			rewrites: {from: '**', to: '/'}
		}
	}
};

if (process.env.NODE_ENV === 'production' || process.env.npm_lifecycle_event === 'build') {
	config.plugins.push(new webpack.optimize.DedupePlugin());
	config.plugins.push(new webpack.optimize.UglifyJsPlugin({
		compress: {
			booleans: true,
			drop_console: true
		}
	}))
}

module.exports = config;